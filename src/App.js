//with help from todos pt1 demo and tj henderson
import React, { useState } from "react";
import todosList from "./todos.json";


function App() {
 
  const [todos, setTodos] = useState(todosList);
  const [input, setInput] = useState("");

  const handleAddTodo = (e) => {
    if(e.which === 13){
      const newId = todos.length + Math.random();
      const newTodo = {
        "userId": 1,
        "id": newId,
        "title": input,
        "completed": false
      }
      
      const newTodos = [
        ...todos,
        newTodo 
      ];
      //newTodos[newId] = newTodo
      setTodos(newTodos);
      setInput("");
    }
  };



  const handleToggle = (id) => {
    const newTodos =[...todos];
    newTodos.forEach(todo => {
      if (todo.id === id){
        todo.completed = !todo.completed;
      }
    });
    //newTodos[id].completed = !newTodos[id].completed;
      setTodos(newTodos);
  };

  const handleDelete = (id) => {
    // const newTodos = [...todos]
    // delete newTodos[id]
    const newTodos = todos.filter(todo => todo.id != id );
    setTodos(newTodos);
  }

  const clearComplete = () => {
      const newTodos = [...todos]
      for (const todo in newTodos) {
        if (newTodos[todo].completed){
          delete newTodos[todo];
        }
      }
      setTodos(newTodos);
  }
console.log(todos);
  return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          onChange={(e)=>setInput(e.target.value)}
          onKeyDown={(e)=>handleAddTodo(e)}
          className="new-todo" 
          value={input}
          placeholder="What needs to be done?" 
          autoFocus />
        </header>
        <TodoList 
        todos={(todos)}
        handleToggle={handleToggle}
        handleDelete={handleDelete}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed"
          onClick= {() => clearComplete()}
          >Clear completed</button>
        </footer>
      </section>
    );
  
}

function TodoItem(props){
    return (
      <li className={props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" checked={props.completed} 
          onChange={() => props.handleToggle(props.id)}
          />
          <label>{props.title}</label>
          <button className="destroy"
          onClick={() => props.handleDelete(props.id)} />
        </div>
      </li>
    );
  
}

function TodoList(props) {
    return (
      <section className="main">
        <ul className="todo-list">
          {props.todos.map((todo) => (
            <TodoItem title={todo.title} completed={todo.completed} 
            id={todo.id}
             handleToggle={props.handleToggle}
             handleDelete={props.handleDelete}
             key={todo.id}
            />
          ))}
        </ul>
      </section>
    );
  }

export default App;
